package com.sk.designpattern.strategy.enums;

import com.google.common.collect.Maps;

import java.util.Map;

/**
 * 渠道枚举
 */
public enum OrgChannelEnum {

    CTRIP("CTRIP", "携程"),
    QUNAR("QUNAR", "去哪儿"),
    ;

    private String code;
    private String name;

    private static final Map<String, OrgChannelEnum> serviceMap = Maps.newHashMap();

    static {
        for (OrgChannelEnum serviceEnum : OrgChannelEnum.values()) {
            serviceMap.put(serviceEnum.getCode(), serviceEnum);
        }
    }

    public static OrgChannelEnum toEnum(String code) {
        return serviceMap.get(code);
    }

    OrgChannelEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("OrgChannelEnum{");
        sb.append("code='").append(code).append('\'');
        sb.append(", name='").append(name).append('\'');
        sb.append('}');
        return sb.toString();
    }

    /**
     * 判断是否为canProcess服务
     */


    public static Boolean isCtrip(String serviceEnum){
        return OrgChannelEnum.CTRIP.getCode().equalsIgnoreCase(serviceEnum);
    }

    public static Boolean isQunar(String serviceEnum){
        return OrgChannelEnum.QUNAR.getCode().equalsIgnoreCase(serviceEnum);
    }
}
