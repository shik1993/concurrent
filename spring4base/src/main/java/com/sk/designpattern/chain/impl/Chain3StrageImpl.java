package com.sk.designpattern.chain.impl;

import com.google.common.collect.Lists;
import com.sk.designpattern.chain.ArgsContext;
import com.sk.designpattern.chain.ChainStrageInterface;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by shik on 2019/6/6.
 */
public class Chain3StrageImpl implements ChainStrageInterface {
    private static final Logger logger = LoggerFactory.getLogger(Chain3StrageImpl.class);

    @Override
    public List executeStrategy(ArgsContext arg) {
        arg.setChain3Result("Chain3StrageImpl#OK");
        logger.info("Chain3StrageImpl#deal {}",arg.getChain3Result());
        return Lists.newArrayList("Chain3StrageImpl");
    }
}
