package com.sk.designpattern.chain.impl;

import com.google.common.collect.Lists;
import com.sk.designpattern.chain.ArgsContext;
import com.sk.designpattern.chain.ChainStrageInterface;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by shik on 2019/6/6.
 */
public class Chain1StrageImpl implements ChainStrageInterface {
    private static final Logger logger = LoggerFactory.getLogger(Chain1StrageImpl.class);

    @Override
    public List executeStrategy(ArgsContext arg) {
        arg.setChain1Result("Chain1StrageImpl#OK");
        logger.info("Chain1StrageImpl#deal {}",arg.getChain1Result());
        return Lists.newArrayList("Chain1StrageImpl");
    }
}
