package com.sk.designpattern.chain;

import com.google.common.collect.Lists;
import com.sk.designpattern.chain.aspectj.ChainMonitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by shik on 2019/6/11.
 */
@Service
public class ChainExecution {
    private static final Logger logger = LoggerFactory.getLogger(ChainExecution.class);

    @Autowired
    private List<ChainStrageInterface> chainStrageInterfaceList;

    /**
     * 这个executeStrategy方法可以作为责任链模式代码的发起源头
     * @param context
     */
    @ChainMonitor
    public List executeStrategyByAnno(ArgsContext context){
        //遍历策略List，执行各个策略
        chainStrageInterfaceList.forEach(item -> {
            try {
                item.executeStrategy(context);
            } catch (Exception exception) {
                logger.error("The CreditCardStrategyChain executeStrategy exception: {}" + exception.getMessage());
            }
        });
        return Lists.newArrayList(context.getChain1Result(),context.getChain4Result());
    }

    /**
     * 这个executeStrategy方法可以作为责任链模式代码的发起源头
     * @param context
     */
    public List executeStrategy(ArgsContext context){
        //遍历策略List，执行各个策略
        chainStrageInterfaceList.forEach(item -> {
            try {
                item.executeStrategy(context);
            } catch (Exception exception) {
                logger.error("The CreditCardStrategyChain executeStrategy exception: {}" + exception.getMessage());
            }
        });
        return Lists.newArrayList(context.getChain1Result(),context.getChain4Result());
    }
}
