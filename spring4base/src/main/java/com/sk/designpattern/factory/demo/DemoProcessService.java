package com.sk.designpattern.factory.demo;

/**
 * Created by shik on 2019/4/24.
 */
public interface DemoProcessService {

    /**
     * 处理业务
     * @param content 请求信息
     * @param service 服务名称
     * @param version 接口版本号
     * @return 业务处理的结果
     */
    public Boolean process(String content, String service, String version);

    /**
     * 该处理器是否可以处理
     *
     * @param serviceName 服务名称，标示请求哪个接口
     * @return true：可以处理；false：否
     */
    public boolean canProcess(String serviceName);
}
