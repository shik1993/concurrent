package com.sk.designpattern.proxy.cglib;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Method;

/**
 * cglib代理工厂类 将目标类(切入点) 和 切面类(通知) 结合===>切面
 * Introduction(引介) 特殊的通知，可以对类增强，添加方法或字段。（知道就行）
 * Created by shik on 2019/6/10.
 */
public class ProxyFactory {
    private static final Logger logger = LoggerFactory.getLogger(ProxyFactory.class);

    /**
     * Aspect：切面，切入点和通知(增强) 本来是互不相互的,可以看做是不同的线，
     * 切入点 和 通知点 多线结合时,就会形成面,也就是我们所说的切面　
     * @return
     */
    //将需要申请代理的类的对象传入,返回一个此对象的代理类对象
    public static Object createProxy(){
        //增强类/切面类
        com.sk.designpattern.proxy.cglib.Myaspect myAspect = new com.sk.designpattern.proxy.cglib.Myaspect();
        //目标类
        DemoServiceImpl demoService = new DemoServiceImpl();

        //使用cglib创建代理类,
        // cglib运行时,动态创建目标类的子类(代理类),所以目标类不可以是final的

        //创建核心类
        Enhancer enhancer = new Enhancer();
        //设置父类
        enhancer.setSuperclass(demoService.getClass());
        //代理类方法将调用回调函数,等效于JDK的 invocationHandler
        //代理类中的Callback,子接口MethodInterceptor对方法进行增强
        /**
         * weaving：织入，将切入点和通知结合，
         * 从没被增强到已经增强的过程
         */
        enhancer.setCallback(new MethodInterceptor() {
            /**
             * weaving：织入，将切入点和通知结合，从没被增强到已经增强的过程
             * @param proxyObj 代理类对象
             * @param method 对象执行的方法
             * @param args 方法入参
             * @param methodProxy 方法代理
             * @return
             * @throws Throwable
             */
            @Override
            public Object intercept(Object proxyObj, Method method, Object[] args, MethodProxy methodProxy) throws Throwable {

                myAspect.beforeInvoke();
                /**
                 * 以下两个方法是等效的: 顺便说明了
                 * cglib运行时,动态创建目标类的子类(代理类),所以目标类不可以是final的
                 * 这句话
                 */
                //Object obj = method.invoke(demoService,args);//执行目标类的方法
                Object obj = methodProxy.invokeSuper(proxyObj,args);//执行代理类(子类)的父类的方法(父类是目标类)
                myAspect.afterInvoke();
                return obj;
            }
        });
        return enhancer.create();
    }
}
