package com.sk.designpattern.proxy.jdk;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 实现类
 * Created by shik on 2019/6/10.
 */
public class DemoServiceImpl implements DemoService {
    private static final Logger logger = LoggerFactory.getLogger(DemoServiceImpl.class);
    @Override
    public void add() {
        logger.info("DemoServiceImpl#jdk proxy#add() ");
    }
}
