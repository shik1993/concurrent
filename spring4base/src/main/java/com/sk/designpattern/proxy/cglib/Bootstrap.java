package com.sk.designpattern.proxy.cglib;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 启动类
 * Created by shik on 2019/4/25.
 */
public class Bootstrap {
    private static final Logger logger = LoggerFactory.getLogger(Bootstrap.class);

    @Test
    public void test() throws InterruptedException {
        logger.debug("---------------------- cglib execute start ! ---------------------- ");
        //通过代理工厂来创建一个代理对象
        DemoServiceImpl demoService = (DemoServiceImpl) ProxyFactory.createProxy();
        //代理对象执行add(),实现方法的增强
        demoService.add();
        logger.debug("---------------------- cglib execute success ! ---------------------- ");
    }
}
