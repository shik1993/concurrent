package com.sk.boot.controller;

import com.sk.boot.config.po.BookComponent;
import com.sk.boot.config.po.BookProperties;
import com.sk.boot.config.po.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 简单风格的一个quickStart
 */
//@Controller
@RestController
public class HelloController {

    /**
     * 这里读取的是  application.properties
     */
    @Autowired
    BookProperties bookProperties;

    @Autowired
    BookComponent bookComponent;

    @Autowired
    Person person;




    //这个类的所有方法返回的数据直接返回给浏览器（如果是对象转为json数据）
    @ResponseBody
    @RequestMapping("/hello")
    public String hello(){
        return "Hello World sk \r\n "
                +"<html><b>hahahah </b></html>"
                +"\r\n bookComponent :" +bookComponent.getName() + " " + bookComponent.getWriter()
                +"\r\n bookProperties :" +bookProperties.getName() + " " + bookProperties.getWriter()
                +"\r\n person :" +person.getLastName() + " " + person.getAge();
    }
}
