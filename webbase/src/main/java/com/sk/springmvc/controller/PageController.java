package com.sk.springmvc.controller;

import com.sk.springmvc.anno.DemoRequestMapping;

public class PageController {

    @DemoRequestMapping(value ="/hello.do")
    public String hello(){
        System.out.println("PageController's hello");
        /**
         * 返回 视图名，DispatcherServlet会依据 视图名定位到具体的jsp页面("/WEB-INF/" + 视图名+".jsp")
         */
        return "hello";
    }

}
