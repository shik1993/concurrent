package com.sk.springmvc;

import com.sk.springmvc.handler.Handler;
import com.sk.springmvc.handler.HandlerMapping;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class DemoDispatcherServlet extends HttpServlet {

    private HandlerMapping handlerMapping;

    //DispatcherServlet的初始化方法
    public void init()throws ServletException {
        /*
		 * 读取smartmvc配置文件中处理器的配置信息，
		 * 然后利用java反射将处理器实例化。
		 */
        //标记1
        System.out.println("className: DemoDispatcherServlet.init");
        SAXReader reader = new SAXReader();
        InputStream in = getClass().getClassLoader().getResourceAsStream("demoSpringMvc.xml");
        try {
            Document doc = reader.read(in);
            Element root = doc.getRootElement();
            List<Element> elements = root.elements();
            //beans用来存放处理器实例
            List beans = new ArrayList();
            for (Element ele : elements) {
                //读取处理器类名
                String className = ele.attributeValue("class");
                System.out.println("className:" + className);
                //将处理器实例化
                Object bean = Class.forName(className).newInstance();
                beans.add(bean);
            }
            //标记2  从标记1到标记2 的代码就是dom4j解析XML文件的固定格式，目的在于将处理类封装在集合里面 统一处理；

            System.out.println("beans:" + beans);//打印验证

            //现在有了处理类（Controller）的集合，那么下面就是统一解析该集合；
            //创建映射处理器实例（DispatcherServlet的成员变量）
            handlerMapping = new HandlerMapping();
            //调用handlerMapping 对象的方法process
            handlerMapping.process(beans);
            //handlerMapping.process(List beans); //解析处理类的集合

        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    protected void service(HttpServletRequest request, HttpServletResponse response)throws ServletException,IOException{
        /*
		 * 先获得请求资源路径，然后截取请求资源路径
		 * 的一部分生成请求路径(path),接下来，
		 * 调用HandlerMapping的getHandler方法来
		 * 获得Handler对象。最后利用Handler对象
		 * 来调用处理器的方法。
		 */
        //获得请求资源路径
        String uri = request.getRequestURI();//smartmvc02/hello.do
        //获得应用名
        String contextPath =request.getContextPath();//smartmvc02
        //截取请求资源路径的一部分(除掉应用名)，
        //生成请求路径。
        String path = uri.substring(contextPath.length());//当时看到这个骚操作我TM惊的一批(hello.do)
        //依据请求路径找到对应的Handler对象。
        Handler handler = handlerMapping.getHandler(path);
        //利用handler对象，调用处理器的方法
        Method mh = handler.getMh();
        Object bean = handler.getObj();
        //rv是处理器方法的返回值(即视图名)。
        Object rv = null;
        try {
            //调用处理器的方法
            rv = mh.invoke(bean);//hello那个方法返回了字符串"hello"
			/*
			 * 依据视图名，生成jsp地址("/WEB-INF/" + 视图名 + ".jsp")
			 */
            String jspPath = "/WEB-INF/springmvc/" + rv + ".jsp";//"/WEB-INF/hello.jsp";
            //转发
            request.getRequestDispatcher(jspPath).forward(request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
