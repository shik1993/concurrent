package com.sk.datastructure.doubleLinkedList;

import com.sk.datastructure.App;
import org.junit.Test;

/**
 * 使用双端链表实现
 * 一个队列， FIFO（先进先出）
 * Created by shik on 2019/2/20.
 */
public class QueueLinkedList implements App {

    private DoublePointLinkedList doublePointLinkedList = new DoublePointLinkedList();

    public QueueLinkedList() {
        this.doublePointLinkedList = (DoublePointLinkedList) doublePointLinkedList.buildDataStracture();
    }

    public DoublePointLinkedList getDoublePointLinkedList() {
        return doublePointLinkedList;
    }

    public void setDoublePointLinkedList(DoublePointLinkedList doublePointLinkedList) {
        this.doublePointLinkedList = doublePointLinkedList;
    }

    public void insert(Object value){
        doublePointLinkedList.addTair(value);
    }

    public void delete(){
        doublePointLinkedList.deleteHead();
    }

    public void display(){
        doublePointLinkedList.display();
    }

    @Override
    public Object buildDataStracture() {
        return null;
    }

    @Override
    @Test
    public void start() {
        // init data
        QueueLinkedList queueLinkedList = new QueueLinkedList();

        queueLinkedList.delete();
        queueLinkedList.display();
        queueLinkedList.insert("W");
        queueLinkedList.display();
    }


}
