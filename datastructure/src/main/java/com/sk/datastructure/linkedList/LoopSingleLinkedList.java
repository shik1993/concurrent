package com.sk.datastructure.linkedList;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by shik on 2019/2/26.
 */
public class LoopSingleLinkedList {
    private static final Logger logger = LoggerFactory.getLogger(LinkedListApp.class);

    private Entry head;

    private int size = 0;

    public Entry getHead() {
        return head;
    }

    public void setHead(Entry head) {
        this.head = head;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    private class Entry{
        int data;
        Entry next;

        public Entry(){
            //初始化的时候，默认设置一个初始值-1，遍历结果时不要打印他
            this.data = -1;
            this.next = null;
        }
        public Entry(int data){
            this.data = data;
            next = null;

        }

        public int getData() {
            return data;
        }

        public void setData(int data) {
            this.data = data;
        }

        public Entry getNext() {
            return next;
        }

        public void setNext(Entry next) {
            this.next = next;
        }

        //添加 toString 会出现 toString（）栈异常问题，我也不知道为啥
//        @Override
//        public String toString() {
//            final StringBuffer sb = new StringBuffer("Entry{");
//            sb.append("data=").append(data);
//            sb.append(", next=").append(next);
//            sb.append('}');
//            return sb.toString();
//        }
//        @Override
//        public String toString() {
//            return "Entry{data="+data+", next="+next+"}";
//        }

    }

    /**
     * 初始化链表，创建一个头节点，next指向自己
     * 如果这个链表里面没有元素，那么就不要执行：        this.head.next = this.head;
     * 因为这么做会爆出栈溢出 异常
     *
     */
    @Test
    public void initLinkedList(){
        head = new Entry();
        this.size++;
        this.head.next = this.head;
        logger.debug("testClink: {}.",head);
    }

    /*
     * 头插法插入结点
     */
    public void headinsert(int val){
        Entry tmp = new Entry(val);
        tmp.next = head.next;
        head.next = tmp;
    }

    /*
     * 尾插法插入结点
     */
    public void tailinsert(int val){
        if(this.size == 0){
            this.initLinkedList();
        }
        Entry entry = new Entry(val);
        Entry tmp = head;
        if(tmp.next == head && this.size == 1){
            head.next = entry;
            entry.next = head;
            size++;
            return ;
        }
        while(tmp.next != head){
            tmp = tmp.next;
        }
        tmp.next = entry;
        entry.next = head;
        size++;

    }

    /*
    获取链表的长度
     */
    public int getlength(){
        return this.size ;
    }

    /*
     判断链表是否为空，即判断头节点的next是否为头节点
      */
    public boolean isEmpty(){
        Entry cur = head;
        if(cur.next != head){
            return false;
        }
        return true;
    }

    /*
     删除一个结点
      */
    public void delete(int val){
        Entry cur;
        Entry prev;
        cur = head.next;
        prev = head;

        while(cur != head){
            if(cur.data == val){
                prev.next = cur.next;
                cur = prev.next;
            }
            cur = cur.next;
            prev = prev.next;
        }
    }

    public void display(){
        if(size == 0){
            logger.info("linklist is empty .");
            return;
        }
        Entry temp = head;
        while (temp.next != head){
            temp = temp.next;
            logger.info("head.data {}",temp.data);
        }

    }

        @Test
        public static void main(String[] args) {
        LoopSingleLinkedList tc = new LoopSingleLinkedList();
        for(int i = 0;i < 10;i++){
            tc.tailinsert(i);
        }
        System.out.println(tc.getlength());
            tc.display();
    }
}
