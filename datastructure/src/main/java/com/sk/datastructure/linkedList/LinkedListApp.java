package com.sk.datastructure.linkedList;

import com.sk.datastructure.App;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * coding by sk
 * Created by shik on 2019/2/19.
 */
public class LinkedListApp implements App{

    private static final Logger logger = LoggerFactory.getLogger(LinkedListApp.class);


    private static Node head = new Node();

    private int length;//链表的长度

    static{
        Node node5 = new Node("5","节点5");
        Node node4 = new Node("4","节点4",node5);
        Node node3 = new Node("3","节点3",node4);
        Node node2 = new Node("2","节点2",node3);
        Node node1 = new Node("1","节点1",node2);
        node1.setFirst(Boolean.TRUE);
        head = node1;
    }

    @Override
    public Object buildDataStracture() {
        return null;
    }

    protected static Node insert(Node node,Node insertNode){
        Node tempNode = node;
        while(tempNode.getNext() != null){
            tempNode = tempNode.getNext();
        }
        if(query(node,tempNode.getKey()) != null)
            query(node,tempNode.getKey()).setNext(insertNode);
        return node;
    }

    protected Node delete(String value){
        return null;
    }

    protected static Node query(Node node,String key){
        Node tempNode = node;

        do{
            if(tempNode.getKey().equalsIgnoreCase(key)){
                return tempNode;
            }
            if(tempNode.getNext() == null){
                return null;
            }
            tempNode = tempNode.getNext();

        }while (!tempNode.getKey().equalsIgnoreCase(key));
        return tempNode;

    }
    public static int getLength(Node node){
        Node tempNode = node;
        AtomicInteger count = new AtomicInteger(1);
        while (tempNode.getNext()!= null){
            tempNode = tempNode.getNext();
            count.incrementAndGet();
        }
        return count.get();
    }


    @Test
    @Override
    public void start() {
        logger.debug("init length {}.",LinkedListApp.getLength(head));
        Node result =LinkedListApp.insert(head,new Node("10","节点10"));
        logger.debug("final length,head {},length {}.",LinkedListApp.getLength(head),LinkedListApp.getLength(result));
        logger.debug(result.toString());
    }
}
