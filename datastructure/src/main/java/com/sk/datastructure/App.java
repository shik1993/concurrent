package com.sk.datastructure;

import org.junit.Test;

/**
 * Created by shik on 2019/2/16.
 */
public interface App {

    /**
     * 构建一个数据结构对象
     */
    public Object buildDataStracture();

    /**
     * 启动数据结构测试用例
     */
    @Test
    public void start();
}
