package com.sk.datastructure.arrays;

/**
 * 二维数组
 */
public class ErWeiArray {

    public static void main(String[] args){
        //方法一：先初始化大小，再赋值，遍历
        int[][] a = new int[3][4];
        for(int i = 0; i <a.length;i++){
            for(int j =0;j< a[i].length;j++){
                a[i][j] = i+j;
            }
        }

        printErWeiArray(a);

        //方法二： 直接赋值
        int[][] b = {
                {1,2,3},
                {2,3,4},
                {4,5,6},
        };

        printErWeiArray(b);
    }

    public static void printErWeiArray(int[][] a){
        System.out.println("^^^^^^^^^^^^^^^^^^^");
        for(int i = 0; i <a.length;i++){
            for(int j =0;j< a[i].length;j++){
                System.out.println(a[i][j]);
            }
        }
    }
}
