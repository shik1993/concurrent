package com.sk.datastructure.queue;

import com.sk.datastructure.linkedList.LinkedListApp;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 循环单向队列
 * Created by shik on 2019/2/25.
 * 使用数组实现
 */
public class MyQueue {

    private static final Logger logger = LoggerFactory.getLogger(LinkedListApp.class);

    //attr
    private Object[]  queue ;
    //队尾
    private int rear;
    //队头
    private int head;
    //队列容器的最大容量
    private int max;
    //队列容器的实际容量
    private int capital;

    public  MyQueue(){
        this.head = 0;
        this.rear = -1;
        this.capital = 0;
        this.max = 5;
        queue = new Object[max];
    }

    /**
     * 队尾加入
     * @param value
     */
    public void insert(Object value){
        if(isFull()){
            logger.info("queue is full, size {}",getSize());
            return;
        }
        ++rear;
        if(rear > max - 1 ){
            rear = 0;
        }
        queue[rear] = value;
        capital++;

    }

    /**
     * 队头移除出队列
     */
    public void remove(){
        if(isEmpty()){
            logger.info("queue is empty, size {}",getSize());
            return;
        }
        queue[head] = null;
        ++head;
        if(head > max - 1){
            head = 0;
        }
        capital--;
    }

    /*
    查看队列头部的数据
     */
    public Object peekFront(){
        return queue[head];
    }

    /**
     * 判断队列是否是空的
     * @return
     */
    public Boolean isEmpty(){
        return capital == 0;
    }

    public Boolean isFull(){
        return capital >= max ;
    }

    public int getRear() {
        return rear;
    }

    public int getHead() {
        return head;
    }

    public int getMax() {
        return max;
    }

    public int getCapital() {
        return capital;
    }

    /**
     * 获取队列的大小
     * @return
     */
    public int getSize(){
        return capital;
    }
    public void display(){
        if(capital <= 0 ){
            logger.info("queue is empty .");
            return;
        }
        logger.info("************display*************");
        for(int i = 0; i < queue.length ; i++){
            logger.info(String.valueOf( queue[i]) );
        }
        logger.info("************display*************");
    }

        @Test
        public void bootstrap(){
            MyQueue queue = new MyQueue();
            queue.display();
            queue.insert(1);
            queue.insert(2);
            queue.insert(3);
            queue.insert(4);
            logger.info("四次插入 ^ head {}, rear {}, peekFront {}",queue.getHead() , queue.getRear(),queue.peekFront());
            queue.display();
            queue.remove();
            logger.info("一次删除 ^ head {}, rear {}, peekFront {}",queue.getHead() , queue.getRear(),queue.peekFront());
            queue.remove();
            queue.display();
            logger.info("第二次删除 ^ head {}, rear {}, peekFront {}",queue.getHead() , queue.getRear(),queue.peekFront());
            queue.insert(5);
            queue.insert(6);
            queue.insert(7);
            queue.insert(8);
            logger.info("再四次插入^ head {}, rear {}, peekFront {}",queue.getHead() , queue.getRear(),queue.peekFront());
            queue.display();

        }
}
