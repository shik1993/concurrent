package com.sk.datastructure.tree;

public class BinNode {

    private String id;

    private String parentId;

    private String text;

    private BinNode leftNode;

    private BinNode rightNode;

    public BinNode(String id, String parentId, String text, BinNode leftNode, BinNode rightNode) {
        this.id = id;
        this.parentId = parentId;
        this.text = text;
        this.leftNode = leftNode;
        this.rightNode = rightNode;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public BinNode getLeftNode() {
        return leftNode;
    }

    public void setLeftNode(BinNode leftNode) {
        this.leftNode = leftNode;
    }

    public BinNode getRightNode() {
        return rightNode;
    }

    public void setRightNode(BinNode rightNode) {
        this.rightNode = rightNode;
    }

    @Override
    public String toString() {
        return "BinNode{" +
                "id='" + id + '\'' +
                ", parentId='" + parentId + '\'' +
                ", text='" + text + '\'' +
                ", leftNode=" + leftNode +
                ", rightNode=" + rightNode +
                '}';
    }
}
