package com.sk.dao;

import com.sk.model.Credit_Request;
import com.sk.model.Credit_RequestExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface Credit_RequestMapper {
    int countByExample(Credit_RequestExample example);

    int deleteByExample(Credit_RequestExample example);

    int deleteByPrimaryKey(Long id);

    int insert(Credit_Request record);

    int insertSelective(Credit_Request record);

    List<Credit_Request> selectByExample(Credit_RequestExample example);

    Credit_Request selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") Credit_Request record, @Param("example") Credit_RequestExample example);

    int updateByExample(@Param("record") Credit_Request record, @Param("example") Credit_RequestExample example);

    int updateByPrimaryKeySelective(Credit_Request record);

    int updateByPrimaryKey(Credit_Request record);
}