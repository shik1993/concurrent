package com.sk.sharding.dao;


import com.sk.sharding.po.CreditRequest;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * 注释掉是因为需要spring + mabatis annotation 支持
 */
@Repository
public interface CreditRequestDao  {

    int updateByPrimaryKeySelective(@Param("record") CreditRequest record);

    CreditRequest queryByOrderNo(@Param("creditRequest") CreditRequest creditRequest);

}