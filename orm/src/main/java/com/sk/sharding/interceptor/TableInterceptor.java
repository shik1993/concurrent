package com.sk.sharding.interceptor;

import com.sk.sharding.po.Entity;
import com.sk.sharding.utils.TableNameUtil;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.ibatis.cache.CacheKey;
import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.plugin.*;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.Properties;

/**
 * 分表拦截器
 */
@Intercepts({@Signature(type = Executor.class, method = "update", args = {MappedStatement.class, Object.class}),
        @Signature(type = Executor.class, method = "query",
                args = {MappedStatement.class, Object.class, RowBounds.class, ResultHandler.class}),
        @Signature(type = Executor.class, method = "query",
                args = {MappedStatement.class, Object.class, RowBounds.class, ResultHandler.class, CacheKey.class, BoundSql.class})})
public class TableInterceptor implements Interceptor {

    private static final Logger logger = LoggerFactory.getLogger(TableInterceptor.class);

    @Override
    public Object intercept(Invocation invocation) throws Throwable {
        Object param = invocation.getArgs()[1];
        if (param != null && param instanceof Map<?, ?> && MapUtils.isNotEmpty ((Map<Object, Object>) param)) {
            for (Map.Entry<Object, Object> entry : ((Map<Object, Object>) param).entrySet()) {
                setTableName(entry.getValue());
            }
        } else if (param != null && param instanceof Entity) {
            setTableName(param);
        }
        return invocation.proceed();
    }

    private void setTableName(Object obj) {
        if (obj != null && obj instanceof Entity) {
            String tableName = TableNameUtil.getTableName(obj);
            if (StringUtils.isNotEmpty(tableName)) {
                ((Entity) obj).setTableName(tableName);
            }
        }
    }

    @Override
    public Object plugin(Object target) {
        return Plugin.wrap(target, this);
    }

    @Override
    public void setProperties(Properties properties) {

    }
}