package com.sk.io;

import java.io.FileOutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

/**
 * nio从代码写入到磁盘文件中
 * 使用NIO写入数据与读取数据的过程类似，同样数据不是直接写入通道，而是写入缓冲区，可以分为下面三个步骤：
 从FileOutputStream获取Channel
 创建Buffer
 将数据从Channel写入到Buffer中

 */
public class NioBufferWrite {
        static private final byte message[] = { 83, 111, 109, 101, 32,
                98, 121, 116, 101, 115, 46 };

        static public void main( String args[] ) throws Exception {
            FileOutputStream fout = new FileOutputStream( "D:\\test.txt" );

            FileChannel fc = fout.getChannel();

            ByteBuffer buffer = ByteBuffer.allocate( 1024 );

            for (int i=0; i<message.length; ++i) {
                buffer.put( message[i] );
            }

            buffer.flip();

            fc.write( buffer );

            fout.close();
        }
    }
