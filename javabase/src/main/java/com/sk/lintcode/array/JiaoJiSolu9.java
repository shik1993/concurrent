package com.sk.lintcode.array;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.collections.MapUtils;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 阿里相互宝第一轮面试题
 * 张三李四喜欢的歌曲如下userSongs
 * 明星:周杰伦,王力宏唱的歌曲如下singerSongs
 * 那么根据这两个List找出张三和李四最喜欢的歌手List(可以是多个)
 */
public class JiaoJiSolu9 {


    public static void main(String[] args){

        Map<String, List<String>> userSongs = Maps.newHashMap();
        userSongs.put("张三", Lists.newArrayList("song1","song2","song3","song4","song8"));
        userSongs.put("李四",Lists.newArrayList("song5","song6","song7"));


        Map<String, List<String>> singerSongs = Maps.newHashMap();
        singerSongs.put("周杰伦",Lists.newArrayList("song1","song3"));
        singerSongs.put("王力宏",Lists.newArrayList("song7"));
        singerSongs.put("孙燕姿",Lists.newArrayList("song2","song4"));
        singerSongs.put("崔健",Lists.newArrayList("song5","song6"));
        singerSongs.put("许巍",Lists.newArrayList("song8","song9"));

        JiaoJiSolu9 jiaoJiSolu9 = new JiaoJiSolu9();
        Map result = jiaoJiSolu9.findSingers(userSongs,singerSongs);
        System.out.print(result);
    }

    public  Map<String, List<String>> findSingers(Map<String, List<String>> userSongs, Map<String, List<String>> singerSongs){
        //默认返回结果
        Map<String, List<String>> result = Maps.newHashMap();
        //入参都是空
        if(MapUtils.isEmpty(singerSongs) && MapUtils.isEmpty(userSongs)){
            return result;
        }
        //入参歌曲为空，但是用户非空
        if(MapUtils.isEmpty(singerSongs)){
            for(String userSong : userSongs.keySet()){
                result.put(userSong,Lists.newArrayList());
            }
            return result;
        }
        //正常场景
        for(String userSong : userSongs.keySet()){
            List<String> userLikeList = userSongs.get(userSong);
            //设置一个temp map：  key：  歌手名， value： 用户喜欢歌曲的 交集的数目
            Map<String,Integer> singerNameMap = Maps.newHashMap();
            for(String singerSong : singerSongs.keySet()){
                List<String>  songsList = singerSongs.get(singerSong);
                //取交集
                List tList = Lists.newArrayList(userLikeList);
                tList.retainAll(songsList);
                singerNameMap.put(singerSong,tList.size());
            }
            //先倒叙 排序，然后 取出singerNameMap中List最多情况下的 key
            List<Map.Entry<String,Integer>> entrys = singerNameMap.entrySet()
                    .stream()
                    .sorted((p1,p2) -> p2.getValue().compareTo(p1.getValue()))
                    .collect(Collectors.toList());
            //遍历entrySets,找到最大数目的歌手的数量： 1-N个
            Iterator it = entrys.iterator();
            Integer temp = -1;
            while(it.hasNext() ){
                Map.Entry entry = (Map.Entry)it.next();
                if(temp == -1){
                    temp = (Integer) entry.getValue();
                    //赋值给result
                    result.put(userSong,Lists.newArrayList((String)entry.getKey()));
                }else if((Integer)entry.getValue() >= temp){
                    //赋值给result
                    List tempList = result.get(userSong);
                    tempList.add(entry.getKey());
                    result.put(userSong,tempList);
                }

            }
        }
        return result;
    }
}
