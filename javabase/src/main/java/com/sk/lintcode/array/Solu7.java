package com.sk.lintcode.array;

public class Solu7 {

    /**
     * 给你两个有序整数数组 nums1 和 nums2，请你将 nums2 合并到 nums1 中，使 num1 成为一个有序数组。
     *
     *  
     *
     * 说明:
     *
     * 初始化 nums1 和 nums2 的元素数量分别为 m 和 n 。
     * 你可以假设 nums1 有足够的空间（空间大小大于或等于 m + n）来保存 nums2 中的元素。
     *  
     *
     * 示例:
     *
     * 输入:
     * nums1 = [1,2,3,0,0,0], m = 3
     * nums2 = [2,5,6],       n = 3
     *
     * 输出: [1,2,2,3,5,6]
     *
     * 来源：力扣（LeetCode）
     * 链接：https://leetcode-cn.com/problems/merge-sorted-array
     * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
     *
     */
    public void merge(int[] nums1,int m , int[] nums2,int n){

        if(nums2.length <= 0){
            return;
        }
        int[] result= new int[m+n];
        int index = 0;
        for(int i=0,j=0;index < (m+n);index++){
            if( i < m && j < n && nums1[i] < nums2[j] ){
                result[index] = nums1[i];
                i++;
            }else if(  i < m && j < n && nums1[i] > nums2[j]){
                result[index] = nums2[j];
                j++;
            }else if(i < m && j < n && nums1[i] == nums2[j] ){
                result[index] = nums1[i];
                i++;
                index++;
                result[index] = nums2[j];
                j++;
            }else if(i >= m){
                result[index] = nums2[j];
                j++;
            }else if(j >= n){
                result[index] = nums1[i];
                i++;
            }
        }

        for(int k = 0 ; k< result.length;k++){
            nums1[k] = result[k];
        }


    }

    public static void main(String[] args){
        Solu7 solu7 = new Solu7();
        solu7.merge(new int[]{1,2,3,0,0,0},3,new int[]{2,5,6},3);
    }

}
