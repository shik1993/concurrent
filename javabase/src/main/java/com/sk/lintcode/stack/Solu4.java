package com.sk.lintcode.stack;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

/**
 * 给定一个只包括 '('，')'，'{'，'}'，'['，']' 的字符串，判断字符串是否有效。
 *
 * 有效字符串需满足：
 *
 * 左括号必须用相同类型的右括号闭合。
 * 左括号必须以正确的顺序闭合。
 * 注意空字符串可被认为是有效字符串。
 *
 * 示例 1:
 *
 * 输入: "()"
 * 输出: true
 * 示例 2:
 *
 * 输入: "()[]{}"
 * 输出: true
 * 示例 3:
 *
 * 输入: "(]"
 * 输出: false
 * 示例 4:
 *
 * 输入: "([)]"
 * 输出: false
 * 示例 5:
 *
 * 输入: "{[]}"
 * 输出: true
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/valid-parentheses
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */
public class Solu4 {

    public boolean isValid(String s) {
        Stack<Character> stack = new Stack<>();
        char[] chars = s.toCharArray();
        for (char aChar : chars) {
            if (stack.size() == 0) {
                stack.push(aChar);
            } else if (isSym(stack.peek(), aChar)) {
                stack.pop();
            } else {
                stack.push(aChar);
            }
        }
        return stack.size() == 0;
    }

    private boolean isSym(char c1, char c2) {
        return (c1 == '(' && c2 == ')') || (c1 == '[' && c2 == ']') || (c1 == '{' && c2 == '}');
    }



    static Map<Character,Character> map = new HashMap<Character,Character>();
    static {
        map.put('{', '}');
        map.put('(', ')');
        map.put('[', ']');
    }
    /**
     * by myself
     * @param s
     * @return
     */
    public boolean isValid1(String s){
        if(s == null || s.length() < 1){
            return true;
        }
        Stack<Character> stack = new Stack<Character>();



        char[] charArray = s.toCharArray();
        for(Character character : charArray){
            if(!stack.isEmpty() && stack.peek() != null && character.equals(map.get(stack.peek()))){
                stack.pop();
            }else{
                stack.push(character);
            }
        }
        if(stack.isEmpty()){
            return true;
        }
        return false;

    }

    public static void main(String[] args){
        Solu4 solu4 = new Solu4();
        System.out.println(solu4.isValid("()"));
        System.out.println(solu4.isValid("(){}[]"));
        System.out.println(solu4.isValid("(]"));
        System.out.println(solu4.isValid("([)]"));
        System.out.println(solu4.isValid("{[()]}"));
    }
}
