package com.sk.jvm;

import java.util.HashMap;
import java.util.Map;

/**
 * 老年代参数设置
 *-Xms30m 初始堆内存大小
 *-Xmx30m 最大堆内存大小
 *-Xmn1m  新生代的大小
 * -XX:MaxTenuringThreshold 执行新生代对象经过多少次gc后进入老年代
 * -XX:PretenureSizeThreshold = 1000 设置对象的大小大于多少时，直接存入老年代
 */
public class HeapTenuredParams {
    /**
     * -Xms30m -Xmx30m -Xmn10m -XX:SurvivorRatio=2 -XX:+PrintGCDetails -XX:+UseSerialGC -XX:PretenureSizeThreshold=1000 -XX:-UseTLAB
     * 这个案例中，我们主要是关闭使用tlab缓存区，同时，设置PretenureSizeThreshold=1000
     * 当 new byte[1088];时，对象直接在堆空间的老年代创建，所以老年代消耗空间大|老年代总20m，用的0.6m 新生代总0.7m，用0.09m
     * 当 new byte[108];时，对象直接在堆空间的新生代创建，所以新生代消耗的空间大|老年代总20m，用了0.1m，新生代总0.7m，用了0.2m
     * @param args
     */
    public static void main(String[] args) {
        Map<Integer, byte[]> m = new HashMap();

        for(int i = 0; i < 5120; ++i) {
            byte[] b = new byte[108];
            m.put(i, b);
        }

    }
}
