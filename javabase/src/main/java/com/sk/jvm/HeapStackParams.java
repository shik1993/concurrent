package com.sk.jvm;

/**
 *
 * java栈参数设置
 * 堆解决的是java实例对象的存储问题，栈解决的是java方法调用的问题
 * 方法调用放在栈中，栈设置的越大，可以调用的越深
 * -Xss5m 设置栈内存大小
 */
public class HeapStackParams {
    private static int count;

    public HeapStackParams() {
    }

    public static void recursion() {
        ++count;
        recursion();
    }

    public static void main(String[] args) {
        try {
            recursion();
        } catch (Throwable var2) {
            System.out.println("调用最大深入：" + count);
            var2.printStackTrace();
        }
    }

}
