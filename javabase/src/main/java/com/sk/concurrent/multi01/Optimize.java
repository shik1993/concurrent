package com.sk.concurrent.multi01;

/**
 * 使用synchronized代码块减小锁的粒度，提高性能
 *
 *
 synchronized(this){

 }
 这里 锁的代码块 其实锁的也是这个对象，而不是单独这一块， 之所以 另一个线程可以进来，是由于 线程具有可重入性。

当前线程开始：t2, 正在执行一个较长时间的业务操作，其内容不需要同步
当前线程开始：t1, 正在执行一个较长时间的业务操作，其内容不需要同步
当前线程：t2, 执行同步代码块，对其同步变量进行操作
当前线程结束：t2, 执行完毕
当前线程：t1, 执行同步代码块，对其同步变量进行操作
当前线程结束：t1, 执行完毕
 */
public class Optimize {

	public void doLongTimeTask(){
		try {
			
			System.out.println("当前线程开始：" + Thread.currentThread().getName() + 
					", 正在执行一个较长时间的业务操作，其内容不需要同步");
			Thread.sleep(2000);
			
			synchronized(this){
				System.out.println("当前线程：" + Thread.currentThread().getName() + 
					", 执行同步代码块，对其同步变量进行操作");
				Thread.sleep(2000);
			}
			System.out.println("当前线程结束：" + Thread.currentThread().getName() +
					", 执行完毕");
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		final Optimize otz = new Optimize();
		final Optimize otz2 = new Optimize();
		Thread t1 = new Thread(new Runnable() {
			@Override
			public void run() {
				otz.doLongTimeTask();
			}
		},"t1");
		Thread t2 = new Thread(new Runnable() {
			@Override
			public void run() {
				otz.doLongTimeTask();
			}
		},"t2");
		Thread t3 = new Thread(new Runnable() {
			@Override
			public void run() {
				otz2.doLongTimeTask();
			}
		},"t3");
		t1.start();
		t2.start();
		t3.start();
		
	}
	
	
}
