package com.sk.concurrent.lock;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * 用两个int变量实现读写锁
 *
 */
public class MyReadWriteLock {

    /**
     * int类型在做++操作时,会出现并发问题,所以可以替换成AtomicInteger
     */
    //private int readcount = 0;
    //private int writecount = 0;
    private AtomicInteger readcount = new AtomicInteger(0);
    private AtomicInteger writecount = new AtomicInteger(0);

    public void lockread() throws InterruptedException{
        while(writecount.get() > 0){
            synchronized(this){
                wait();
            }
        }
        //readcount++;
        readcount.incrementAndGet();
        //进行读取操作
        System.out.println("读操作 readcount:"+readcount);
    }

    public void unlockread(){
        //readcount--;
        readcount.decrementAndGet();
        System.out.println("读锁解操作 readcount:"+readcount);
        synchronized(this){
            notifyAll();
        }
    }

    public void lockwrite() throws InterruptedException{
        while(writecount.get() > 0){
            synchronized(this){
                wait();
            }
        }
        //之所以在这里先++，是先占一个坑，避免读操作太多，从而产生写的饥饿等待
        //writecount++;
        writecount.incrementAndGet();
        while(readcount.get() > 0){
            synchronized(this){
                wait();
            }
        }
        //进行写入操作
        System.out.println("写操作 writecount:"+ writecount);
    }

    public void unlockwrite(){
        //writecount--;
        writecount.decrementAndGet();
        System.out.println("写锁解操作 writecount:"+writecount);
        synchronized(this){
            notifyAll();
        }
    }

    public static void main(String[] args) throws InterruptedException {
        MyReadWriteLock readWriteLock = new MyReadWriteLock();
        for(int i = 0; i < 2; i++){
            Thread2 thread2 = new Thread2(i, readWriteLock);
            thread2.start();
        }

        for (int i = 0; i < 10; i++) {
            Thread1 thread1 = new Thread1(i, readWriteLock);
            thread1.start();
        }

    }

}

class Thread1 extends Thread{
    public int i;
    public MyReadWriteLock readWriteLock;

    public Thread1(int i, MyReadWriteLock readWriteLock) {
        this.i = i;
        this.readWriteLock = readWriteLock;
    }

    @Override
    public void run() {
        try {
            readWriteLock.lockread();
            //Thread.sleep(1000);//模拟耗时
            System.out.println("第"+i+"个读任务");
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            readWriteLock.unlockread();
        }
    }
}


class Thread2 extends Thread{
    public int i;
    public MyReadWriteLock readWriteLock;

    public Thread2(int i, MyReadWriteLock readWriteLock) {
        this.i = i;
        this.readWriteLock = readWriteLock;
    }

    @Override
    public void run() {
        try {
            readWriteLock.lockwrite();
            //Thread.sleep(1000);
            System.out.println("第"+i+"个写任务");
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            readWriteLock.unlockwrite();
        }
    }
}