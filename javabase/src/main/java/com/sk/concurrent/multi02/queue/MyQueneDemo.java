package com.sk.concurrent.multi02.queue;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * 可阻塞式的队列
 * Created by shik on 2019/2/16.
 */
public class MyQueneDemo {
    private static final Logger logger = LoggerFactory.getLogger(MyQueneDemo.class);

    //对象锁
    public Object lock = new Object();
    //队列容量设定
    public static final int initCapacity = 5;
    //定义一个队列，容量是5
    public volatile List list = new ArrayList(initCapacity);

    public int size(){
        return list.size();
    }

    @Test
    public void testInitList(){
        logger.info("list.size() :"+list.size());
        logger.info("list.toString() :"+list.get(1));
    }

    /**
     * 可阻塞式的方法put，想队列的尾巴插入数据
     * @param value
     * @return
     */
    public int put(int value){

        synchronized (lock){
            if( list.size()> initCapacity ){
                try{
                    lock.wait();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }


        }

        return value;
    }

    /**
     * 可阻塞式的队列方法take 从队列的头部拿数据
     * @return
     */
    public int take(){

        return 0;
    }
}
