package com.sk.concurrent.multi02.wait_notify;

import java.util.ArrayList;
import java.util.List;

/**
 * 模拟  thread 的 wait和 notify 功能
 * thread1   处理业务，到了某个阶段，就发出通知，
 * thread2  监听消息， 收到后做自己的逻辑。
 * 这么做的缺点：
 * 1. thread2长期 空挂，占用内存
 * 2. thread1不停的执行，也不会停（因为tread2抛出了运行时异常，但是只是把自己给终止了）
 */
public class ListAdd1 {

	private volatile static List list = new ArrayList();	
	
	public void add(){
		list.add("bjsxt");
	}
	public int size(){
		return list.size();
	}
	
	public static void main(String[] args) {
		
		final ListAdd1 list1 = new ListAdd1();
		
		Thread t1 = new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					for(int i = 0; i <10; i++){
						list1.add();
						System.out.println("当前线程：" + Thread.currentThread().getName() + "添加了一个元素..");
						Thread.sleep(500);
					}	
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}, "t1");
		
		Thread t2 = new Thread(new Runnable() {
			@Override
			public void run() {
				while(true){
					if(list1.size() == 5){
						System.out.println("当前线程收到通知：" + Thread.currentThread().getName() + " list size = 5 线程停止..");
						throw new RuntimeException();
					}
				}
			}
		}, "t2");		
		
		t1.start();
		t2.start();
	}
	
	
}
